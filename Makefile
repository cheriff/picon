
all: picon

picon: main.o
	gcc -o $@ main.o -L. -lftd2xx

run: picon
	./picon
