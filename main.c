#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

#include <pthread.h>

#include "ftd2xx.h"

static FT_HANDLE handle = NULL;
static void close_ftdi(void) { if (handle) { printf("Closing FTDI\n"); FT_Close(handle); }}

static inline int min(int a, int b) { if (a<b) return a; else return b;}

static void*
ftdi_input_thread(void *arg)
{
    static DWORD buffsz = 1024;
    DWORD data_read;
    char *buffer = malloc(buffsz);
    FT_STATUS status;

    EVENT_HANDLE eh;
    pthread_mutex_init(&eh.eMutex, NULL);
    pthread_cond_init(&eh.eCondVar, NULL);

    DWORD EventMask = FT_EVENT_RXCHAR | FT_EVENT_MODEM_STATUS;
    status = FT_SetEventNotification(handle, EventMask, (PVOID)&eh);
    assert(status == FT_OK);


    while(1) {
        pthread_mutex_lock(&eh.eMutex);
        printf("Waiting\n");
        pthread_cond_wait(&eh.eCondVar, &eh.eMutex);
        pthread_mutex_unlock(&eh.eMutex);

        DWORD EventDWord;
        DWORD RxBytes;
        DWORD TxBytes;
        DWORD Status;
        FT_GetStatus(handle,&RxBytes,&TxBytes,&EventDWord);
        printf("GOT %d %d %x\n", RxBytes, TxBytes, EventDWord);

        while (RxBytes) {
            int amt = min(RxBytes, buffsz);
            status = FT_Read(handle, buffer, amt, &data_read);
            if (status != FT_OK) {
                printf("READ ERROR: %d\n", status);
                return (void*)(uintptr_t)status;
            }
            for(int i=0; i<data_read; i++)
                putchar(buffer[i]);
            RxBytes -= data_read;
        }
    }
    return NULL;
}

static void *
user_thread(void *arg)
{
    while(1) {
        char c = getchar();
        printf("C IS '%c'\n", c);
        if (c == '') {
            printf("down\n");
            assert(FT_SetDtr(handle) == FT_OK);
            usleep(100000);
            printf("up\n");
            assert(FT_ClrDtr(handle) == FT_OK);
        }
    }
    return NULL;
}

int
main(int argc, char *argv[])
{
    FT_STATUS status;
    DWORD num_devices;

    status = FT_CreateDeviceInfoList(&num_devices);
    assert(status == FT_OK);

    if (num_devices == 0) {
        printf("No devices found, exiting\n");
        return -1;
    }

    printf("Have  %d devices\n", num_devices);

    FT_DEVICE_LIST_INFO_NODE *nodes;
    nodes = calloc(num_devices, sizeof(*nodes));
    printf("have nodes %p\n", nodes);

    status = FT_GetDeviceInfoList(nodes, &num_devices);
    assert(status == FT_OK);


    for (int i=0; i<num_devices; i++) {
        printf("Device %d/%d\n", i+1, num_devices);
        printf("  Serial: %s\n", nodes[i].SerialNumber);
        printf("  Desc  : %s\n", nodes[i].Description);
        printf("  %x %x %d %d\n",
                nodes[i].Flags,
                nodes[i].Type,
                nodes[i].ID,
                nodes[i].LocId);
    }

    int target=0;

    status = FT_Open(target, &handle);
    assert(status == FT_OK);

    assert(FT_SetTimeouts(handle, 100, 100) == FT_OK);
    assert(FT_SetFlowControl(handle, FT_FLOW_NONE,0,0) == FT_OK);
    assert(FT_SetBaudRate(handle, FT_BAUD_115200) == FT_OK);

    pthread_t thread;
    status = pthread_create(&thread, NULL, ftdi_input_thread, NULL);
    printf("Created %d\n", status);

    user_thread(NULL);
    printf("And finished\n");
    return status;
}


